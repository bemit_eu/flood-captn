<?php

namespace Flood;

class Captn {
    const TYPE__DEFAULT = '0';
    const TYPE__COMPONENT = '100';
    const TYPE__HOOK = '500';
    const TYPE__LIB = '1000';
}