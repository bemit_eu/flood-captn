<?php

spl_autoload_register(function($class) {
    $prefix = 'Flood\Captn\\';
    $prefix_length = strlen($prefix);
    if(strncmp($prefix, $class, $prefix_length) !== 0) {
        return;
    }
    $src_path = 'src/';
    $class_path = __DIR__ . '/' . $src_path . str_replace('\\', '/', substr($class, $prefix_length)) . '.php';
    if(file_exists($class_path)) {
        require $class_path;
    }
});

spl_autoload_register(function($class) {
    if('Flood\Captn' === $class) {
        require __DIR__ . '/Captn.php';
    }
});

/**
 * To use in components which should have some Captn routine in them (auto register, acknowledge) but that should be only triggered when captn is in use.
 *
 * Turn on captn with defining the constant `CAPTN_IS_STEERING` as `true`
 *
 * @example if(function_exists('captnIsSteering') && captnIsSteering()){}
 *
 * @return bool
 */
function captnIsSteering() {
    if(
        defined('CAPTN_IS_STEERING') &&
        true === CAPTN_IS_STEERING
    ) {
        // captn is enabled
        return true;
    }

    return false;
}