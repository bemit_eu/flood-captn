<?php

namespace Flood\Captn;

/**
 * Class Acknowledge
 *
 * @package Flood\Captn
 * @todo    try add caching on the foreach in onEachTyped and onEachSignaled
 */
class Acknowledge {

    protected static $signaled = [];
    protected static $type_index = [];

    public static function signal($id, $id_unique, $type, $value) {
        if(!isset(static::$signaled[$id])) {
            static::$signaled[$id] = [];
        }
        if(isset(static::$signaled[$id][$id_unique])) {
            // todo: currently removes all types for type and not only the `id` and/or matching `id_unique`
            unset(static::$type_index[static::$signaled[$id][$id_unique]['t']]);
        }
        static::$signaled[$id][$id_unique]['v'] = $value;
        static::$signaled[$id][$id_unique]['t'] = $type;
        static::$type_index[$type][$id][$id_unique] =& static::$signaled[$id][$id_unique];
    }

    public static function has($id, $id_unique = false)
    : bool {
        if(false === $id_unique) {
            if(isset(static::$signaled[$id])) {
                return true;
            }
        } else if(isset(static::$signaled[$id][$id_unique])) {
            return true;
        }

        return false;
    }

    public static function hasType($type, $id = false, $id_unique = false)
    : bool {
        if(false === $id) {
            if(isset(static::$type_index[$type])) {
                return true;
            }
        } else {
            if(false === $id_unique) {
                if(isset(static::$type_index[$type][$id])) {
                    return true;
                }
            } else if(isset(static::$type_index[$type][$id][$id_unique])) {
                return true;
            }
        }

        return false;
    }

    /**
     * The value array is passed as reference to the callable, this is done for every item in the array
     *
     * @param      $callable
     * @param      $id
     * @param bool $id_unique
     *
     * @todo currently fails silently
     */
    public static function onEachSignaled($callable, $id = false, $id_unique = false) {
        if(false === $id) {
            foreach(static::$signaled as $in_id) {
                foreach($in_id as $id_u => $value) {
                    call_user_func_array($callable, [$value['v'], $value['t']]);
                }
            }
        }
        if(static::has($id, $id_unique)) {
            if(false === $id_unique) {
                foreach(static::$signaled[$id] as $id_u => $value) {
                    call_user_func_array($callable, [$value['v']]);
                }
            } else {
                call_user_func_array($callable, [static::$signaled[$id][$id_unique]['v']]);
            }
        }
    }

    /**
     * The value array is passed as reference to the callable, this is done for every item in the array
     *
     * @param      $type
     * @param      $callable
     * @param      $id
     * @param bool $id_unique
     *
     * @todo currently fails silently
     */
    public static function onEachTyped($callable, $type = false, $id = false, $id_unique = false) {
        if(false === $type) {
            foreach(static::$type_index as $in_type => $in_1) {
                foreach($in_1 as $in_id => $in_2) {
                    foreach($in_2 as $id_u => $value) {
                        call_user_func_array($callable, [$value['v'], $value['t'], $in_id, $id_u]);
                    }
                }
            }
        }

        if(static::hasType($type, $id, $id_unique)) {
            if(false === $id) {
                foreach(static::$type_index[$type] as $in_id => $in_1) {
                    foreach($in_1 as $id_u => $value) {
                        call_user_func_array($callable, [$value['v'], $value['t'], $in_id, $id_u]);
                    }
                }
            } else {
                if(false === $id_unique) {
                    foreach(static::$type_index[$type][$id] as $id_u => $value) {
                        call_user_func_array($callable, [$value['v'], $value['t'], $id, $id_u]);
                    }
                } else {
                    call_user_func_array($callable, [static::$type_index[$type][$id][$id_unique]['v'], static::$type_index[$type][$id][$id_unique]['t'], $id, $id_unique]);
                }
            }
        }
    }

    /**
     * @param      $id
     * @param bool $id_unique
     *
     * @return array
     * @todo trying to not-use getters here
     */
    /*public static function getSignaled($id = false, $id_unique = false)
    : array {
        if (false === $id) {
            return static::$signaled;
        }
        if (static::has($id, $id_unique)) {
            if (false === $id_unique) {
                return static::$signaled[$id];
            } else {
                return static::$signaled[$id][$id_unique];
            }
        }
        return [];
    }*/

    /**
     * @param      $type
     * @param bool $id
     * @param bool $id_unique
     *
     * @return array
     * @todo trying to not-use getters here
     */
    /*public static function getTyped($type, $id = false, $id_unique = false)
    : array {
        if (static::hasType($type, $id, $id_unique)) {
            if (false === $id) {
                return static::$type_index[$type];
            } else {
                if (false === $id_unique) {
                    return static::$type_index[$type][$id];
                }

                return static::$type_index[$type][$id][$id_unique];
            }
        }
        return [];
    }*/
}