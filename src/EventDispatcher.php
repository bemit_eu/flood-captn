<?php

namespace Flood\Captn;

class EventDispatcher {

    /**
     * @var array
     */
    protected static $event_list = [];

    /**
     * @param $id
     * @param $callable
     */
    public static function on($id, $callable) {
        if(!isset(static::$event_list[$id])) {
            static::$event_list[$id] = [];
        }
        if(is_callable($callable)) {
            static::$event_list[$id][] = $callable;
        } else {
            error_log('Flood\Captn: Error in EventDispatcher, nothing to execute for ' . $id);
        }
    }

    /**
     * @param      $id
     */
    public static function off($id) {
        if(isset(static::$event_list[$id])) {
            unset(static::$event_list[$id]);
        }
    }

    /**
     * @param $id
     * @param $data
     *
     * @return array
     */
    public static function trigger($id, $data = []) {
        $result = $data;
        if(isset(static::$event_list[$id]) && is_array(static::$event_list[$id])) {

            foreach(static::$event_list[$id] as $callable) {
                $result = call_user_func_array($callable, [$result]);
            }
        }

        return $result;
    }
}